# wildfly-singleton-service

**Deploy cluster-wide singleton services (as CDI beans or EJBs) on WildFly.**


## How?
``@Start`` annotated methods will be called when your application starts, but
only on one WildFly instance. If this instance becomes unavailable for some reason
(e.g. application is undeployed, server is shutdown or crashes, etc...), another
WildFly instance will take over (aka "failover") by calling the ``@Start`` annotated
methods there.

``@Stop`` annotated methods are supposed to be called on a WildFly instance that
was providing the service but no longer should do so (e.g. cluster topology changes,
application being undeploy on that instance, etc...). But this has been somewhat 
unreliable in my experience (at least on WildFly 8 and 9).


## Supported bean types
* CDI beans
  * ``@Dependent``
  * ``@ApplicationScoped``
* EJBs
  * ``@Stateless``
  * ``@Singleton``


## Supported WildFly versions
* WildFly 10.1
  * use version 1.2.1
    * supports [multiple deployments](#multiple-deployments) on same application server
    * uses default singleton policy (see [HA Singleton Features](https://docs.jboss.org/author/display/WFLY10/HA+Singleton+Features#HASingletonFeatures-Configuration))
* WildFly 10
  * use version 1.1.0
* WildFly 9
  * use version 1.0.1

## Example
```xml
<dependency>
    <groupId>com.bertoncelj.wildflysingletonservice</groupId>
    <artifactId>wildfly-singleton-service</artifactId>
    <version>1.2.1</version>
</dependency>
```

```java
@ApplicationScoped
public class ExampleApplicationScopedBean {
    @Inject
    Logger log;

    @Start
    public void start() {
        log.info("Service starting.");
    }

    @Stop
    public void stop() {
        log.info("Service stopping.");
    }
}
```

## Multiple deployments
__This works since version 1.2.0 only.__

If you'd like to use this library in more than one application at once (deployed on the same server), you need to make
MSC service name unique for each application.

Add a resource file *resources/wildfly-singleton-service.properties* to your project:
```properties
name=foobar
```

In this example ``foobar`` would be appended to default MSC service name so the end result would be ``wildfly.singleton.service.foobar``