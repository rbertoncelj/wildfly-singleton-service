package com.bertoncelj.wildflysingletonservice;

import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * @author Rok Bertoncelj
 */
@WebServlet("/*")
public class MyServiceServlet extends HttpServlet {
    @Inject
    MyService myService;

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        resp.getOutputStream().println(myService.isRunning());
    }
}
