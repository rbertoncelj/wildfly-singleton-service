package com.bertoncelj.wildflysingletonservice;

import javax.enterprise.context.ApplicationScoped;

/**
 * @author Rok Bertoncelj
 */
@ApplicationScoped
public class MyService {
    boolean running;

    @Start
    public void start() {
        running = true;
    }

    @Stop
    public void stop() {
        running = false;
    }

    public boolean isRunning() {
        return running;
    }
}
