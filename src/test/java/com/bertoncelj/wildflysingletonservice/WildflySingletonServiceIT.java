package com.bertoncelj.wildflysingletonservice;

import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.container.test.api.RunAsClient;
import org.jboss.arquillian.container.test.api.TargetsContainer;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.api.asset.StringAsset;
import org.jboss.shrinkwrap.api.spec.WebArchive;
import org.jboss.shrinkwrap.impl.base.io.IOUtil;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.net.HttpURLConnection;
import java.net.URL;

import static org.junit.Assert.*;

/**
 * @author Rok Bertoncelj
 */
@SuppressWarnings("ArquillianTooManyDeployment")
@RunWith(Arquillian.class)
public class WildflySingletonServiceIT {

    @TargetsContainer("wf1")
    @Deployment(testable = false, name = "wf1")
    public static WebArchive getDeployment1() {
        return getDeployment();
    }

    @TargetsContainer("wf2")
    @Deployment(testable = false, name = "wf2")
    public static WebArchive getDeployment2() {
        return getDeployment();
    }

    public static WebArchive getDeployment() {
        try {
            WebArchive webArchive = ShrinkWrap.create(WebArchive.class, "test.war")
                    .addPackages(true, "com.bertoncelj.wildflysingletonservice")
                    .deleteClass(WildflySingletonServiceIT.class)
                    .setManifest(new StringAsset("Dependencies: org.slf4j,org.jboss.msc,org.jboss.as.clustering.common,org.jboss.as.clustering.infinispan,org.jboss.as.server"))
                    .addAsResource("wildfly-singleton-service.properties")
                    .addAsResource("META-INF/beans.xml")
                    .addAsResource("META-INF/services/javax.enterprise.inject.spi.Extension")
                    .addAsResource("META-INF/services/org.jboss.msc.service.ServiceActivator")
                    .addAsWebInfResource(new StringAsset("<jboss-web><context-root>/test</context-root></jboss-web>"), "jboss-web.xml");
            System.out.println(webArchive.toString(true));
            return webArchive;
        } catch (Exception e) {
            e.printStackTrace();
            throw e;
        }
    }

    @RunAsClient
    @Test
    public void testStuff() throws Exception {
        HttpURLConnection connection1 = (HttpURLConnection) new URL("http://localhost:8080/test").openConnection();
        assertEquals(HttpURLConnection.HTTP_OK, connection1.getResponseCode());
        String responseMessage1 = IOUtil.asUTF8String(connection1.getInputStream());
        boolean active1 = responseMessage1.contains("true");

        HttpURLConnection connection2 = (HttpURLConnection) new URL("http://localhost:8230/test").openConnection();
        assertEquals(HttpURLConnection.HTTP_OK, connection2.getResponseCode());
        String responseMessage2 = IOUtil.asUTF8String(connection2.getInputStream());
        boolean active2 = responseMessage2.contains("true");

        assertTrue(active1 || active2);
        assertFalse(active1 && active2);
    }
}