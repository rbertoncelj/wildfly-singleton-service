package com.bertoncelj.wildflysingletonservice;

import org.jboss.msc.service.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.wildfly.clustering.singleton.SingletonPolicy;
import org.wildfly.clustering.singleton.SingletonRequirement;

/**
 * @author Rok Bertoncelj
 */
public class WildflySingletonServiceActivator implements ServiceActivator {
    private static final Logger log = LoggerFactory.getLogger(WildflySingletonServiceActivator.class);

    @Override
    public void activate(ServiceActivatorContext context) throws ServiceRegistryException {
        log.info("Installing {}", WildflySingletonService.class.getSimpleName());

        WildflySingletonService wildflySingletonService = new WildflySingletonService();
        try {
            ServiceName singletonPolicyServiceName = ServiceName.parse(SingletonRequirement.SINGLETON_POLICY.getDefaultRequirement().getName());
            SingletonPolicy policy = (SingletonPolicy) context.getServiceRegistry()
                    .getRequiredService(singletonPolicyServiceName)
                    .awaitValue();
            policy.createSingletonServiceBuilder(WildflySingletonService.getServiceName(), wildflySingletonService)
                    .build(context.getServiceTarget())
                    .install();
        } catch (InterruptedException e) {
            throw new ServiceRegistryException(e);
        } catch (ServiceNotFoundException e) {
            log.warn("Singleton policy is not available. Check if you are running with HA configuration. Service will be enabled on this node!");
            try {
                wildflySingletonService.start(null);
            } catch (StartException e1) {
                throw new ServiceRegistryException("Unable to start service locally (without HA configuration)", e);
            }
        }

        log.debug("Installed {}", WildflySingletonService.class.getSimpleName());
    }
}
