package com.bertoncelj.wildflysingletonservice;

import org.jboss.msc.service.ServiceName;
import org.jboss.msc.service.StartContext;
import org.jboss.msc.service.StartException;
import org.jboss.msc.service.StopContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

/**
 * @author Rok Bertoncelj
 */
public class WildflySingletonService implements org.jboss.msc.service.Service<String> {
    public static final ServiceName DEFAULT_NAME = ServiceName.of("wildfly", "singleton", "service");
    public static final String PROPERTIES_RESOURCE = "wildfly-singleton-service.properties";
    public static final String NAME_PROPERTY = "name";

    private static final Logger log = LoggerFactory.getLogger(WildflySingletonService.class);

    private static ServiceName serviceName;

    public static ServiceName getServiceName() {
        if (serviceName == null) {
            serviceName = DEFAULT_NAME;
            InputStream inputStream = WildflySingletonServiceActivator.class.getResourceAsStream("/" + PROPERTIES_RESOURCE);
            if (inputStream != null) {
                Properties properties = new Properties();
                try {
                    properties.load(inputStream);
                    String name = properties.getProperty(NAME_PROPERTY);
                    if (name != null) {
                        serviceName = DEFAULT_NAME.append(name.split("\\."));
                    }
                    log.info("Using custom service name: {}", serviceName);
                } catch (IOException e) {
                    log.error("Could not load properties", e);
                }
            }
        }
        return serviceName;
    }

    @Override
    public void start(StartContext context) throws StartException {
        log.info("This node ({}) should now provide singleton services.", System.getProperty("jboss.node.name"));
        try {
            Controller.getInstance().startServices();
        } catch (IllegalStateException e) {
            log.debug("CDI not initialized yet. Services will be started later.");
            log.trace("Could not obtain an instance of Controller", e);
        }
    }

    @Override
    public void stop(StopContext context) {
        log.info("This node ({}) should not provide singleton services anymore.", System.getProperty("jboss.node.name"));
        Controller.getInstance().stopServices();
    }

    @Override
    public String getValue() throws IllegalStateException, IllegalArgumentException {
        return System.getProperty("jboss.node.name");
    }
}
