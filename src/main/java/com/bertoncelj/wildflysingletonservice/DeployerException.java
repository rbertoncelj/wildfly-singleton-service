package com.bertoncelj.wildflysingletonservice;

/**
 * @author Rok Bertoncelj
 */
public class DeployerException extends RuntimeException {
    public DeployerException(String message, Object... arguments) {
        super(String.format(message, arguments));
    }
}
