package com.bertoncelj.wildflysingletonservice;

import javax.enterprise.inject.spi.Bean;
import java.lang.reflect.Method;

/**
 * Singleton service descriptor, Contains information such as which method on which bean to call when
 * a service needs to be started or stopped.
 *
 * @author Rok Bertoncelj
 */
public class Service<T> {
    private Bean<?> bean;
    private Method startMethod;
    private Method stopMethod;

    public Service(Bean<?> bean, Method startMethod, Method stopMethod) {
        this.bean = bean;
        this.startMethod = startMethod;
        this.stopMethod = stopMethod;
    }

    public Bean<?> getBean() {
        return bean;
    }

    public Method getStartMethod() {
        return startMethod;
    }

    public Method getStopMethod() {
        return stopMethod;
    }

    @Override
    public String toString() {
        return "Service{" +
                "bean=" + bean.getBeanClass().getSimpleName() +
                ", startMethod=" + startMethod.getName() +
                ", stopMethod=" + stopMethod.getName() +
                '}';
    }

//    private String beanToString(boolean useSimpleName) {
//        StringBuilder sb = new StringBuilder();
//        for (Annotation annotation : bean.getQualifiers()) {
//            sb.append("@")
//                    .append(useSimpleName
//                            ? annotation.getClass().getSimpleName()
//                            : annotation.getClass().getName())
//                    .append(" ");
//        }
//        sb.append(useSimpleName
//                ? bean.getBeanClass().getSimpleName()
//                : bean.getBeanClass().getName());
//        return sb.toString();
//    }
}
