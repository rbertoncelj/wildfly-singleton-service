package com.bertoncelj.wildflysingletonservice;

import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import static java.lang.annotation.ElementType.METHOD;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

/**
 * Marks a method on a bean as a service "stop" method. This method might be called when local server node
 * should not be providing the service anymore (e.g. during application shutdown, splitbrain merge, etc...).
 * <p/>
 * Note that during my testing, this was somewhat inconsistent and thus unreliable... :-(
 *
 * @author Rok Bertoncelj
 */
@Target(METHOD)
@Retention(RUNTIME)
public @interface Stop {
}
